package com.yelyzaveta.currency.main.controllers;

import com.yelyzaveta.currency.main.core.CurrencyPrognoserFacade;
import com.yelyzaveta.currency.main.dto.Result;
import com.yelyzaveta.currency.main.services.CurrencyService;
import com.yelyzaveta.currency.main.services.DayService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;

@Controller
@RequestMapping("/currency")
public class CurrencyController {


    private final CurrencyPrognoserFacade currencyPrognoserFacade;
    private final CurrencyService currencyService;
    private final DayService dayService;

    public CurrencyController(CurrencyPrognoserFacade currencyPrognoserFacade, CurrencyService currencyService, DayService dayService) {
        this.currencyPrognoserFacade = currencyPrognoserFacade;
        this.currencyService = currencyService;
        this.dayService = dayService;
    }

    @PostMapping("/{id}")
    public Result calculate(Model model,
                            @PathVariable int id,
                            @ModelAttribute LocalDate date) {
        return currencyPrognoserFacade.prognose(date, id);
    }

    @GetMapping()
    public String getCurrency(Model model) {
        model.addAttribute("currency", currencyService.getAll());
        return "currency";
    }

    @GetMapping("/{id}")
    public String chooseDay(Model model, @PathVariable int id) {
        model.addAttribute("min", LocalDate.now().plusDays(1));
        model.addAttribute("max", dayService.getMax().getDate().plusDays(dayService.getCount() / 3));
        return "days";
    }
}
