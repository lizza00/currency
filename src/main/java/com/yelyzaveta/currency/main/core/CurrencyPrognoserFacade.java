package com.yelyzaveta.currency.main.core;

import com.yelyzaveta.currency.main.dto.LinearCoeficients;
import com.yelyzaveta.currency.main.dto.Result;
import com.yelyzaveta.currency.main.entities.CurrencyDay;
import com.yelyzaveta.currency.main.services.CurrencyDayService;
import com.yelyzaveta.currency.main.services.CurrencyService;
import com.yelyzaveta.currency.main.services.DayService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CurrencyPrognoserFacade {

    private final CurrencyDayService currencyDayService;
    private final CurrencyService currencyService;
    private final DayService dayService;

    public CurrencyPrognoserFacade(CurrencyDayService currencyDayService,
                                   CurrencyService currencyService,
                                   DayService dayService) {
        this.currencyDayService = currencyDayService;
        this.currencyService = currencyService;
        this.dayService = dayService;
    }

    public Result prognose(LocalDate prognoseDate, int id) {

        long difference = getPrognosePeriod(prognoseDate);
        List<CurrencyDay> data = currencyDayService.getAllByDateAfter(dayService.getByDate(LocalDate.now().minusDays(difference * 3 + 1)), currencyService.getById(id));
        List<Double> covznaAverage = getCovznaAverage(data);
        List<Double> centeredCovznaAverage = getCenteredCovznaAverage(covznaAverage);
        List<Double> markOfComponent = getMarkOfComponent(data, centeredCovznaAverage);
        List<Double> fixedComponent = getFixedComponent(markOfComponent);
        double k = getFixedCoeficient(fixedComponent);
        List<Double> s = fixedComponent;
        for (int i = 0; i < markOfComponent.size() / 4; i++) {
            s.addAll(fixedComponent);
        }
        s = s.stream().map(d -> d - k).collect(Collectors.toList());
        List<Double> tPlusE = getTPlusE(data, s);
        LinearCoeficients coeficients = getLinearCoeficients(tPlusE);
        List<Double> t = getT(data.size(), coeficients);
        List<Double> tPlusS = getTPlusS(t, s);
        List<Double> e = getE(data, tPlusS);
        List<Double> e2 = getE2(e);
        List<Double> prognosedData = getPrognosedData(data.size(), s, coeficients);
        return Result.builder()
                .difference(difference)
                .data(data)
                .covznaAverage(covznaAverage)
                .centeredCovznaAverage(centeredCovznaAverage)
                .markOfComponent(markOfComponent)
                .fixedComponent(fixedComponent)
                .k(k)
                .s(s)
                .tPlusE(tPlusE)
                .coeficients(coeficients)
                .t(t)
                .tPlusS(tPlusS)
                .e(e)
                .e2(e2)
                .prognosedData(prognosedData)
                .build();
    }

    private long getPrognosePeriod(LocalDate prognoseDate) {
        return ChronoUnit.DAYS.between(LocalDate.now(), prognoseDate);
    }

    private List<Double> getCovznaAverage(List<CurrencyDay> data) {
        List<Double> result = new ArrayList<>();
        for (int i = 1; i < data.size() - 2; i++) {
            result.add(data.subList(i - 1, i + 2).stream().mapToDouble(CurrencyDay::getValue).average().getAsDouble());
        }
        return result;
    }

    private List<Double> getCenteredCovznaAverage(List<Double> data) {
        List<Double> result = new ArrayList<>();
        for (int i = 1; i < data.size(); i++) {
            result.add(data.subList(i - 1, i).stream().mapToDouble(d -> d).average().getAsDouble());
        }
        return result;
    }

    private List<Double> getMarkOfComponent(List<CurrencyDay> data, List<Double> data2) {
        List<Double> result = new ArrayList<>();
        for (int i = 2; i < data.size()-2; i++) {
            result.add(data.get(i).getValue() - data2.get(i - 2));
        }
        return result;
    }

    private List<Double> getSubstract(List<CurrencyDay> data, List<Double> data2) {
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            result.add(data.get(i).getValue() - data2.get(i));
        }
        return result;
    }

    private List<Double> getFixedComponent(List<Double> data) {
        List<Double> result = new ArrayList<>();
        List<Double> res = new ArrayList<>();
        result.add((double) 0);
        result.add((double) 0);
        result.addAll(data);
        result.add((double) 0);
        result.add((double) 0);
        for (int j = 0; j < result.size() / 4; j++) {
            double sum = 0;
            for (int i = j; i < result.size(); i += 4) {
                sum += result.get(i);
            }
            res.add(sum / result.size() / 4 - 1);
        }
        return res;
    }

    private double getFixedCoeficient(List<Double> data) {
        return data.stream().mapToDouble(d -> d).sum()/4;
    }

    private List<Double> getTPlusE(List<CurrencyDay> data1, List<Double> data2) {
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < data1.size(); i++) {
            result.add(data1.get(i).getValue() - data2.get(i));
        }
        return result;
    }

    private LinearCoeficients getLinearCoeficients(List<Double> data) {
        double y1 = data.get(0);
        double y2 = data.get(data.size() - 1);
        double x1 = 1;
        double x2 = data.size() - 1;
        LinearCoeficients result = new LinearCoeficients();
        result.setA((y1 - y2) / (x1 - x2));
        result.setB(y1 - result.getA() * x1);
        return result;
    }

    private List<Double> getT(int count, LinearCoeficients coeficients) {
        List<Double> result = new ArrayList<>();
        for (int i = 1; i <= count; i++) {
            result.add(coeficients.getA() * count + coeficients.getB());
        }
        return result;
    }

    private List<Double> getTPlusS(List<Double> data1, List<Double> data2) {
        List<Double> result = new ArrayList<>();
        for (int i = 0; i < data1.size(); i++) {
            result.add(data1.get(i) + data2.get(i));
        }
        return result;
    }

    private List<Double> getE(List<CurrencyDay> data, List<Double> data2) {
        return getSubstract(data, data2);
    }

    private List<Double> getE2(List<Double> e) {
        return e.stream().map(d -> d * d).collect(Collectors.toList());
    }

    private List<Double> getPrognosedData(int count, List<Double> s, LinearCoeficients coeficients) {
        List<Double> result = new ArrayList<>();
        for (int i = count + 1; i < count * 4 / 3; i++) {
            result.add(coeficients.getA() * i + coeficients.getB() + s.get(i - count - 1));
        }
        return result;
    }
}
