package com.yelyzaveta.currency.main.dto;

import lombok.Data;

@Data
public class LinearCoeficients {
    private double a;

    private double b;
}
