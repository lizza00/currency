package com.yelyzaveta.currency.main.dto;

import com.yelyzaveta.currency.main.entities.CurrencyDay;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Result {

    private double k;
    private long difference;
    private List<CurrencyDay> data;
    private List<Double> prognosedData;
    private List<Double> covznaAverage;
    private List<Double> centeredCovznaAverage;
    private List<Double> markOfComponent;
    private List<Double> fixedComponent;
    private List<Double> s;
    private List<Double> tPlusE;
    private LinearCoeficients coeficients;
    private List<Double> t;
    private List<Double> tPlusS;
    private List<Double> e;
    private List<Double> e2;
}
