package com.yelyzaveta.currency.main.repositories;

import com.yelyzaveta.currency.main.entities.Currency;
import com.yelyzaveta.currency.main.entities.CurrencyDay;
import com.yelyzaveta.currency.main.entities.Day;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CurrencyDayRepository extends CrudRepository<CurrencyDay,Integer> {

    List<CurrencyDay> findAllByDayAfterAndCurrency(Day day, Currency currency);
}
