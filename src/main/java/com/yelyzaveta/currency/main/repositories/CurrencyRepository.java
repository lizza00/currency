package com.yelyzaveta.currency.main.repositories;

import com.yelyzaveta.currency.main.entities.Currency;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends CrudRepository<Currency,Integer> {
    Currency findDistinctByCode(String code);
}
