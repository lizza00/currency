package com.yelyzaveta.currency.main.repositories;

import com.yelyzaveta.currency.main.entities.CurrencyDay;
import com.yelyzaveta.currency.main.entities.Day;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface DayRepository extends CrudRepository<Day, Integer> {

    @Query("SELECT d FROM Day d WHERE d.date = (SELECT MAX(d2.date) FROM Day d2)")
    Day findByMaxDay();

    @Query("SELECT d FROM Day d WHERE d.date = (SELECT MIN(d2.date) FROM Day d2)")
    Day findByMinDay();

    Day findByDate(LocalDate localDate);

    @Query("SELECT COUNT(d) FROM Day d")
    Integer findCount();

}
