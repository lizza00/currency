package com.yelyzaveta.currency.main.services;

import com.yelyzaveta.currency.main.entities.Currency;
import com.yelyzaveta.currency.main.entities.CurrencyDay;
import com.yelyzaveta.currency.main.entities.Day;
import com.yelyzaveta.currency.main.repositories.CurrencyDayRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class CurrencyDayService {

    private final CurrencyDayRepository currencyDayRepository;


    public CurrencyDayService(CurrencyDayRepository currencyDayRepository) {
        this.currencyDayRepository = currencyDayRepository;
    }

    @Transactional
    public void save(CurrencyDay currencyDays) {
        currencyDayRepository.save(currencyDays);
    }


    @Transactional(readOnly = true)
    public List<CurrencyDay> getAllByDateAfter(Day day, Currency currency){
        return currencyDayRepository.findAllByDayAfterAndCurrency(day, currency);
    }
}
