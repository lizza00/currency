package com.yelyzaveta.currency.main.services;

import com.yelyzaveta.currency.main.entities.Currency;
import com.yelyzaveta.currency.main.repositories.CurrencyRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CurrencyService {

    private final CurrencyRepository currencyRepository;


    public CurrencyService(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Transactional
    public void saveAll(List<Currency> currency) {
        currencyRepository.saveAll(currency);
    }


    @Transactional(readOnly = true)
    public Currency getByCode(String code) {
        return currencyRepository.findDistinctByCode(code);
    }

    @Transactional(readOnly = true)
    public Currency getById(int id){
        return currencyRepository.findById(id).get();
    }

    @Transactional(readOnly = true)
    public List<Currency> getAll(){
        return (List<Currency>) currencyRepository.findAll();
    }
}
