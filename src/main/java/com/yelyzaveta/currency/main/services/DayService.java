package com.yelyzaveta.currency.main.services;

import com.yelyzaveta.currency.main.entities.Day;
import com.yelyzaveta.currency.main.repositories.DayRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
public class DayService {

    private final DayRepository dayRepository;

    public DayService(DayRepository dayRepository) {
        this.dayRepository = dayRepository;
    }

    @Transactional
    public void save(Day day) {
        dayRepository.save(day);
    }

    @Transactional(readOnly = true)
    public Day getMax() {
        return dayRepository.findByMaxDay();
        // return new Day(5000,LocalDate.parse("2018-12-31"));
    }

    @Transactional(readOnly = true)
    public Day getMin() {
        return dayRepository.findByMinDay();
//        return new Day(1, LocalDate.parse("2017-12-14"));
    }

    @Transactional(readOnly = true)
    public Day getByDate(LocalDate localDate) {
        return dayRepository.findByDate(localDate);
    }

    @Transactional(readOnly = true)
    public Integer getCount() {
        return dayRepository.findCount();
    }
}
