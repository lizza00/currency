package com.yelyzaveta.currency.parser.controllers;

import com.yelyzaveta.currency.main.entities.Currency;
import com.yelyzaveta.currency.main.entities.CurrencyDay;
import com.yelyzaveta.currency.main.entities.Day;
import com.yelyzaveta.currency.main.services.CurrencyDayService;
import com.yelyzaveta.currency.main.services.CurrencyService;
import com.yelyzaveta.currency.main.services.DayService;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/parse")
public class ParserController {

    private final CurrencyService currencyService;

    @Autowired
    private final CurrencyDayService currencyDayService;

    private final DayService dayService;

    public ParserController(CurrencyService currencyService, CurrencyDayService currencyDayService, DayService dayService) {
        this.currencyService = currencyService;
        this.currencyDayService = currencyDayService;
        this.dayService = dayService;
    }

    @GetMapping("/codes")
    public boolean parseCodes() {
        try {
            Document doc = Jsoup.connect("https://bank.gov.ua/markets/exchangerates?date=04.11.2000&period=daily")
                    .userAgent("Chrome/4.0.249.0 Safari/532.5")
                    .referrer("http://www.google.com")
                    .get();
            Elements listOfCurrency = doc.select("table#exchangeRates");
            List<Currency> currencies = new ArrayList<>();
            for (Element element : listOfCurrency.select("td")) {
                String code = element.getElementsByAttributeValue("data-label", "Код літерний").text();
                if (!StringUtils.isBlank(code)) {
                    currencies.add(Currency.builder()
                            .code(code)
                            .build());
                }
            }
            int i = 0;
            for (Element element : listOfCurrency.select("td.value-name")) {
                currencies.get(i).setName(element.getElementsByTag("a").text());
            }
            currencyService.saveAll(currencies);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @GetMapping("/days")
    public boolean parseDays() {
        LocalDate localDate = LocalDate.parse("2000-01-01");
        LocalDate compared = LocalDate.parse("2020-01-01");
        while (localDate.isBefore(compared)) {
            dayService.save(Day.builder()
                    .date(localDate)
                    .build());
            localDate = localDate.plusDays(1);
        }
        return true;
    }

    @GetMapping("/currency")
    public boolean parseCurrency() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        String url = "https://bank.gov.ua/markets/exchangerates?date=%s&period=daily";
        LocalDate maxDate = dayService.getMax().getDate();
        try {
            for (LocalDate localDate = dayService.getMin().getDate();
                 !localDate.isAfter(maxDate); localDate = localDate.plusDays(1)) {
                Document doc = Jsoup.connect(String.format(url, localDate.format(dateTimeFormatter)))
                        .userAgent("Chrome/4.0.249.0 Safari/532.5")
                        .referrer("http://www.google.com")
                        .get();
                Elements listOfCurrency = doc.select("table#exchangeRates");
                for (Element element : listOfCurrency.select("tr")) {
                    String code = element.getElementsByAttributeValue("data-label", "Код літерний").text();
                    String course = element.getElementsByAttributeValue("data-label", "Курс").text();
                    String count = element.getElementsByAttributeValue("data-label", "Кількість одиниць").text();
                    if (!StringUtils.isBlank(code)) {
                        currencyDayService.save(CurrencyDay.builder()
                                .day(dayService.getByDate(localDate))
                                .currency(currencyService.getByCode(code))
                                .value(Double.parseDouble(course.replace(',','.'))/Double.parseDouble(count))
                                .build());
                    }

                }
                System.out.println(localDate);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
