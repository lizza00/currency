<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Currency</title>
</head>
<body>
<table align="center">
    <tr><th>Code</th><th>Name</th></tr>
    <c:forEach items="${currency}" var="c">

        <tr>

            <td> <a href="currency/${c.getId()}">${c.getCode()}  </a></td>
            <td>${c.getName()}</td>

        </tr>
    </c:forEach>
</table>
</body>
</html>
